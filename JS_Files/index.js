let squares = {};
let columns = {};
let tops = {};
let chart = document.getElementById("chart")
let canMove = true
let gameOn = true
console.log("hello");

(function fillSquaresArray() {
    for (let i = 0; i < 7; i++) {
        let column = document.createElement("div")
        column.className = "flexColumn chartColumn"
        column.id = "c" + i
        let topCell = document.createElement("div")
        topCell.className = "topSquare"
        topCell.id = "t" + i
        column.appendChild(topCell)
        for (let j = 0; j < 7; j++) {
            let cell = document.createElement('div')
            cell.id = "c" + i + "r" + j
            cell.className = "square"
            column.appendChild(cell)
            squares["c" + i + "r" + j] = cell
        }
        chart.appendChild(column)
        columns["c" + i] = document.getElementById("c" + i)
        columns["c" + i]["floor"] = 6
        let circleImage = createPiece(false, true);
        tops[i] = document.getElementById("t" + i)
        tops[i]["blue"] = circleImage
        tops[i].appendChild(circleImage)
        columns["c" + i].addEventListener("mouseover", () => {
            if (canMove)
                tops[i]["blue"].style.display = "block"
        })
        columns["c" + i].addEventListener("mouseout", () => tops[i]["blue"].style.display = "none")
        columns["c" + i].addEventListener("click", () => {
            if (canMove)
                oneTurn(i)

        })
    }
})();


function createPiece(visible, player) {
    let circleImage;
    circleImage = document.createElement("img");
    if (player)
        circleImage.src = "circle.png";
    else
        circleImage.src = "circleRed.png";
    circleImage.className = "circle"
    if (!visible)
        circleImage.style.display = "none"
    return circleImage
}

function transform(index, player, color) {
    let length = columns["c" + index]["floor"] * 53
    tops[index][color].style.transform = `translateY(${length}px)`;
    tops[index][color].style.transition = "all 0.5s linear"
    let randColumn = Math.round(Math.random() * 6)
    setTimeout(() => {
        tops[index].removeChild(tops[index][color])
        movePiece(index, player)
        length = columns["c" + randColumn]["floor"] * 53
        if (player) {
            let circleImage = createPiece(true, !player);
            tops[randColumn]["red"] = circleImage
            tops[randColumn].appendChild(circleImage)
            circleImage = createPiece(false, true);
            tops[index]["blue"] = circleImage
            tops[index].appendChild(circleImage)
        }

    }, 550)
    return randColumn
}


function movePiece(column, player) {
    let index = "c" + column + "r" + columns["c" + column]["floor"]
    squares[index].appendChild(createPiece(true, player))
    squares[index]["color"] = player ? "blue" : "red"
    let result = checkLine(columns["c" + column]["floor"], column, player ? "blue" : "red")
    if (result) {
        gameOn = false
        let text = player ? "blue won" : "red won"
        let textNode = document.createTextNode(text)
        document.getElementById("content").appendChild(textNode)
    }
    columns["c" + column]["floor"]--
    return result
}

function oneTurn(index) {
    canMove = false
    let column
    if (gameOn)
        column = transform(index, true, "blue")
    else return
    setTimeout(() => {
        if (gameOn)
            transform(column, false, "red")
    }, 1500)
    setTimeout(() => canMove = true, 2500)
}


function checkLine(row, column, color) {
    if (row < 4) {
        for (let i = row; i < 7; i++) {
            let index = "c" + column + "r" + i
            if (squares[index]["color"] === color) {
                if (i == row + 3) {
                    return true
                }
            } else {
                break
            }
        }
    }

    let count = 0
    for (let i = Math.max(0, column - 3); i <= Math.min(6, column + 3); i++) {
        let index = "c" + i + "r" + row
        if (squares[index]["color"] === color) {
            count++
            if (count === 4) {
                return true
            }
        } else {
            count = 0
        }
    }


    let i = row + 1
    let j = column + 1
    let index = "c" + j + "r" + i
    count = 0
    while (i < 7 && j < 7 && squares[index]["color"] === color && squares[index]["color"]!== undefined) {
        index = "c" + j + "r" + i
        count++
        i++
        j++
    }
    i = row - 1
    j = column - 1
    index = "c" + j + "r" + i
    while (i >= 0 && j >= 0 && squares[index]["color"] === color  && squares[index]["color"]!== undefined) {
        index = "c" + j + "r" + i
        count++
        i--
        j--
    }
    if (count + 1 > 3)
        return true

    i = row + 1
    j = column - 1
    index = "c" + j + "r" + i
    count = 0
    while (i < 7 && j >= 0 && squares[index]["color"] === color  && squares[index]["color"]!== undefined) {
        index = "c" + j + "r" + i
        count++
        i++
        j--
    }
    i = row - 1
    j = column + 1
    index = "c" + j + "r" + i
    while (i >= 0 && j < 7 && squares[index]["color"] === color  && squares[index]["color"]!== undefined) {
        index = "c" + j + "r" + i
        count++
        i--
        j++
    }

    return count + 1 > 3;

}





